#!/usr/bin/bash

function scriptinator_icon_set { 
	echo "{PlasmoidIconStart}$@{PlasmoidIconEnd}";
};

function scriptinator_status_set { 
	echo "{PlasmoidStatusStart}$@{PlasmoidStatusEnd}";
};

if [[ "$1" == "status" ]]; then
	if [[ "$(virsh -c qemu:///system domstate debian12)" == "running" ]]; then
		scriptinator_icon_set "/home/tubbadu/Pictures/icons/virt-manager-running.svg"
		scriptinator_status_set "active"
	else
		scriptinator_icon_set "/home/tubbadu/Pictures/icons/virt-manager-stopped.svg"
		scriptinator_status_set "hidden"
	fi
fi

if [[ "$1" == "toggle" ]]; then
	if [ "$(virsh -c qemu:///system domstate debian12)" = "running" ]; then
		virsh -c qemu:///system shutdown debian12
		
		for i in {1..30}; do
			if [[ "$(virsh -c qemu:///system domstate debian12)" != "running" ]]; then
				scriptinator_icon_set "/home/tubbadu/Pictures/icons/virt-manager-stopped.svg"
				exit 0  
			fi
			sleep 1 
		done
		scriptinator_icon_set "data-error"
		exit 1
	else
		virsh -c qemu:///system start debian12
		
		for i in {1..45}; do
			if [[ "$(virsh -c qemu:///system domstate debian12)" == "running" ]]; then
				scriptinator_icon_set "/home/tubbadu/Pictures/icons/virt-manager-running.svg"
				exit 0  
			fi
			sleep 1 
		done
		scriptinator_icon_set "data-error"
		exit 1
	fi
fi


